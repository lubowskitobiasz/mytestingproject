from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


from django import forms

class CalculatorForm(forms.Form):
    dlugosc = forms.FloatField(label='Długość budynku')
    szerokosc = forms.FloatField(label='Szerokość budynku')
    pietro = forms.ChoiceField(label='Piętro/Parter', choices=[('parter', 'Parter'), ('pietro', 'Piętrowy')], widget=forms.RadioSelect)
    material = forms.ChoiceField(label='Materiał', choices=[('cegła', 'Ściany z cegły'), ('beton', 'Ściany z betonu komórkowego'),('pustaki', 'Ściany z pustaków')], widget=forms.RadioSelect)


class RoofForm(forms.Form):
    TYPY_DACHU = [
        ('kopertowy', 'Dach kopertowy'),
        ('dwuspadowy', 'Dach dwuspadowy'),
    ]

    typ_dachu = forms.ChoiceField(label='Typ dachu', choices=TYPY_DACHU, widget=forms.RadioSelect)
    dlugosc = forms.FloatField(label='Długość')
    szerokosc = forms.FloatField(label='Szerokość')

