from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from . forms import UserRegisterForm
from . forms import CalculatorForm
from . forms import RoofForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .models import Walls



def home(request):
    return render(request, 'users/home.html')


def register(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Hi {username}, your account was created successfully')
            return redirect('home')
    else:
        form = UserRegisterForm()

    return render(request, 'users/register.html', {'form': form})


@login_required()
def profile(request):
    return render(request, 'users/profile.html')

def calculate(request):
    form = CalculatorForm(request.POST or None)
    result = None

    if form.is_valid():
        dlugosc = form.cleaned_data['dlugosc']
        szerokosc = form.cleaned_data['szerokosc']
        pietro = form.cleaned_data['pietro']
        material = form.cleaned_data['material']

        if pietro == 'pietro':
            wynik = dlugosc * szerokosc * 1.5
        else:
            wynik = dlugosc * szerokosc

        if material == 'cegła':
            cena_materialu = 5
        elif material == 'beton':
            cena_materialu = 10
        elif material == 'pustaki':
            cena_materialu = 15
        # Dodaj inne opcje cen materiałów według potrzeb

        wynik *= cena_materialu

        result = wynik

    context = {
        'form': form,
        'result': result
    }

    return render(request, 'users/calculate.html', context)


def roof(request):
    if request.method == 'POST':
        form = RoofForm(request.POST)
        if form.is_valid():
            # Pobierz dane z formularza
            typ_dachu = form.cleaned_data['typ_dachu']
            dlugosc = form.cleaned_data['dlugosc']
            szerokosc = form.cleaned_data['szerokosc']

            # Wykonaj obliczenia dla dachu

            # Przygotuj dane do wyświetlenia na nowej stronie
            context = {
                'typ_dachu': typ_dachu,
                'dlugosc': dlugosc,
                'szerokosc': szerokosc,
            }

            return render(request, 'users/roof_result.html', context)
    else:
        form = RoofForm()
    return render(request, 'users/roof.html', {'form': form})

def save_project(request):
    # Przetwarzanie zapisu projektu

    return render(request, 'users/save_project.html')



def calculate(request):
    if request.method == 'POST':
        form = CalculatorForm(request.POST)
        if form.is_valid():
            # Pobierz dane z formularza
            dlugosc = form.cleaned_data['dlugosc']
            szerokosc = form.cleaned_data['szerokosc']
            pietro = form.cleaned_data['pietro']
            material = form.cleaned_data['material']

            # Utwórz obiekt Walls i zapisz go w bazie danych
            walls = Walls(dlugosc=dlugosc, szerokosc=szerokosc, pietro=pietro, material=material)
            walls.save()

            # Przekaz dane do widoku roof_result
            request.session['dlugosc'] = dlugosc
            request.session['szerokosc'] = szerokosc

            return redirect('roof_result')

    else:
        form = CalculatorForm()

    return render(request, 'calculate.html', {'form': form})



def roof_result(request):
    dlugosc = request.session.get('dlugosc')
    szerokosc = request.session.get('szerokosc')

    # Pobierz dane z tabeli Walls na podstawie ostatnio zapisanego rekordu
    walls = Walls.objects.latest('id')  # Zakładam, że id jest poliem auto-increment w tabeli

    # Wykonaj obliczenia na podstawie danych z walls
    wynik = dlugosc * walls.szerokosc * (1 if walls.pietro == 'parter' else 1.5) * walls.material

    return render(request, 'roof_result.html', {'wynik': wynik, 'dlugosc': dlugosc, 'szerokosc': szerokosc})
