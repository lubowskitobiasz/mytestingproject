from django.db import models

class Walls(models.Model):
    dlugosc = models.FloatField()
    szerokosc = models.FloatField()
    pietro = models.CharField(max_length=20, choices=[('parter', 'Parter'), ('pietro', 'Piętrowy')], default='parter')
    material = models.CharField(max_length=20, choices=[('cegła', 'Ściany z cegły'), ('beton', 'Ściany z betonu komórkowego'),('pustaki', 'Ściany z pustaków')], default='cegła')

    def __str__(self):
        return f'Walls: {self.dlugosc} x {self.szerokosc}'

