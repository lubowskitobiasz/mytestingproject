from django.urls import path
from . import views
from django.contrib.auth import views as auth_view



urlpatterns = [
    path('', views.home, name='home'),
    path('register/', views.register, name='register'),
    path('profile/', views.profile, name='profile'),
    path('calculate/', views.calculate, name='calculate'),
    path('login/', auth_view.LoginView.as_view(template_name='users/login.html'), name="login"),
    path('logout/', auth_view.LogoutView.as_view(template_name='users/logout.html'), name="logout"),
    path('roof/', views.roof, name='roof'),
    path('save_project/', views.save_project, name='save_project'),
]
