from django.db import models

class Project(models.Model):
    name = models.CharField(max_length=100)
    roof_type = models.CharField(max_length=100)
    length = models.DecimalField(max_digits=10, decimal_places=2)
    width = models.DecimalField(max_digits=10, decimal_places=2)
    # Dodaj inne pola, które są potrzebne do przechowywania informacji o projekcie

    def __str__(self):
        return self.name
